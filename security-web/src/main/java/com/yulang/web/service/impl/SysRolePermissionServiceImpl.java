package com.yulang.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.web.mapper.SysRoleMapper;
import com.yulang.web.mapper.SysRolePermissionMapper;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysRolePermission;
import com.yulang.web.service.SysPermissionService;
import com.yulang.web.service.SysRolePermissionService;
import com.yulang.web.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {
}
