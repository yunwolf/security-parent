package com.yulang.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.web.mapper.SysPermissionMapper;
import com.yulang.web.mapper.SysRoleMapper;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.service.SysPermissionService;
import com.yulang.web.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {


    @Autowired
    private SysPermissionMapper mapper;

    @Transactional
    @Override
    public List<SysPermission> selectPermissionByUserId(String userId){
        List<SysPermission> sysPermissions = mapper.selectPermissionByUserId(userId);
        return sysPermissions;
    }

    @Transactional
    @Override
    public boolean deleteById(Long id) {
        mapper.deleteById(id);
        LambdaQueryWrapper<SysPermission> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(SysPermission::getId,id);
        mapper.delete(queryWrapper);
        return true;
    }

    @Override
    public List<SysPermission> findPermissionByRoleId(String roleId) {
        return mapper.findPermissionByRoleId(roleId);
    }
}
