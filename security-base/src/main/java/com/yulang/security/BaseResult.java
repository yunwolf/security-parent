package com.yulang.security;

public class   BaseResult {


    private BaseResult(){};


    private Integer code;

    private String message;

    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public static BaseResult ok(){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(200);
        baseResult.setMessage("成功响应！");
        return baseResult;
    }

    public static BaseResult ok(String message){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(200);
        baseResult.setMessage(message);
        return baseResult;
    }

    public static BaseResult ok(Object data){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(200);
        baseResult.setMessage("成功响应！");
        baseResult.setData(data);
        return baseResult;
    }

    public static BaseResult error(String message){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(500);
        baseResult.setMessage(message);
        return baseResult;
    }

    public static BaseResult error(Integer code, String message){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(code);
        baseResult.setMessage(message);
        return baseResult;
    }


}
