package com.yulang.web.security;

import com.yulang.security.core.mobile.SendSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 用来替换默认实现
 */
@Component
public class MobileSmsCodeSender implements SendSms {

    Logger logger = LoggerFactory.getLogger(MobileSmsCodeSender.class);

    @Override
    public boolean sendSms(String mobile, String content) {
        logger.info(content);
        // todo 调用具体的实现
        return true;
    }
}
