package com.yulang.security.core.listener;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationSuccessListener {

   void successListener(HttpServletRequest request, HttpServletResponse response, Authentication authentication);

}
