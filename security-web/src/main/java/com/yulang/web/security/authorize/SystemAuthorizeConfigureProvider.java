package com.yulang.web.security.authorize;

import com.yulang.security.core.provider.AuthorizeConfigureProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

@Component
public class SystemAuthorizeConfigureProvider implements AuthorizeConfigureProvider {

    @Override
    public void configure(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
        // registry.antMatchers("/user").hasAnyAuthority("sys:user");
    }
}
