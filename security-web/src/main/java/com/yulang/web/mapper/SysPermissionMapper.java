package com.yulang.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysRole;

import java.util.List;

public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermission> selectPermissionByUserId(String userId);
    List<SysPermission> findPermissionByRoleId(String roleId);

}
