package com.yulang.web.exception;

import com.yulang.security.BaseResult;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionAdvice {

    @ExceptionHandler({BusinessException.class})
    public BaseResult execption(Exception e){
        return BaseResult.error(HttpStatus.BAD_REQUEST.value(),e.getMessage());
    }

}
