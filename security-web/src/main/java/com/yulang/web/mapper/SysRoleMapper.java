package com.yulang.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.web.pojo.SysRole;

import java.util.List;

public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> findRoleByUserId(String userId);

}
