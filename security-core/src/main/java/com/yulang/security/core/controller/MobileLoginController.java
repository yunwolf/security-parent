package com.yulang.security.core.controller;

import com.yulang.security.BaseResult;
import com.yulang.security.core.mobile.SendSms;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 手机登录控制层,前后端分离的可以省略
 */
@Controller
public class MobileLoginController {

    public static final String SESSION_KEY = "SESSION_KEY_MOBILE_CODE";

    @Autowired
    private SendSms sendSms;

    /**
     * 跳转到登录页
     * @return
     */
    @RequestMapping("/mobile/page")
    public String mobilePage(){
        return "login-mobile";
    }

    /**
     * 发送手机验证码
     */
    @ResponseBody
    @RequestMapping("/code/mobile")
    public BaseResult smsCode(HttpServletRequest request){
        //生成手机验证码
        String s = RandomStringUtils.randomNumeric(4);
        //将手机验证码存到session中
        request.getSession().setAttribute(SESSION_KEY,s);
        //调用接口发送验证码
        sendSms.sendSms(request.getParameter("mobile"),s);
        return BaseResult.ok();
    }


}
