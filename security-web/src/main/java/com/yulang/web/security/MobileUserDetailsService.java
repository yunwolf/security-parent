package com.yulang.web.security;

import com.yulang.web.pojo.SysUser;
import com.yulang.web.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MobileUserDetailsService extends AbstractUserDetailFactory {

    @Autowired
    private SysUserService sysUserService;

    @Override
    SysUser findSysUser(String userNameOrMobile) {
        return sysUserService.findByPhone(userNameOrMobile);
    }
}
