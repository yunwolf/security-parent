package com.yulang.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysRolePermission;

public interface SysRolePermissionService extends IService<SysRolePermission> {

}
