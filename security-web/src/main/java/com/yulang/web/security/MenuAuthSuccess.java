package com.yulang.web.security;

import com.alibaba.fastjson.JSONObject;
import com.yulang.security.core.listener.AuthenticationSuccessListener;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MenuAuthSuccess implements AuthenticationSuccessListener {

    private Logger logger = LoggerFactory.getLogger(MenuAuthSuccess.class);

    @Override
    public void successListener(HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authentication) {
        logger.info("--------------");
        Object principal = authentication.getPrincipal();
        if (principal != null && principal instanceof SysUser) {
            SysUser sysUser = (SysUser) principal;
            List<SysPermission> permissions = sysUser.getPermissions();
            List<SysPermission> sysPermissions = permissions.stream().filter(sysPermission -> sysPermission.getType() == 1).collect(Collectors.toList());
            sysUser.setPermissions(sysPermissions);
        }
    }
}
