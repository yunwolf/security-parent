package com.yulang.web.security;

import com.yulang.web.exception.BusinessException;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysUser;
import com.yulang.web.service.SysPermissionService;
import com.yulang.web.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 用户的信息获取方式应该又指定的服务处理
 */
@Component
public class CustomUserService extends AbstractUserDetailFactory {

    @Autowired
    private SysUserService sysUserService;

    @Override
    SysUser findSysUser(String userNameOrMobile) {
        return sysUserService.findByUsername(userNameOrMobile);
    }
}
