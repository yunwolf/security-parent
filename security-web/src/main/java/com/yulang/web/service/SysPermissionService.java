package com.yulang.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.web.pojo.SysPermission;
import com.yulang.web.pojo.SysUser;

import java.util.List;

public interface SysPermissionService extends IService<SysPermission> {


    public List<SysPermission> selectPermissionByUserId(String userId);

    boolean deleteById(Long id);

    public List<SysPermission> findPermissionByRoleId(String roleId);

}
