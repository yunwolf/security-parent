package com.yulang.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysRolePermission;

public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {
}
