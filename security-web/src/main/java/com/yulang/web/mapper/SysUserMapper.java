package com.yulang.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.web.pojo.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
