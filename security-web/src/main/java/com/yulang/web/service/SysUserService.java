package com.yulang.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.web.pojo.SysUser;

public interface SysUserService extends IService<SysUser> {

    SysUser findByUsername(String username);

    SysUser findByPhone(String mobile);

    void saveOrUpdates(SysUser sysUser);

    void delete(Long id);
}
