package com.yulang.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysUserRole;

import java.util.List;

public interface SysUserRoleService extends IService<SysUserRole> {
}
