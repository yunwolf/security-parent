package com.yulang.web;

import com.yulang.web.service.SysPermissionService;
import com.yulang.web.service.SysUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class test {


    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysPermissionService sysPermissionService;

    @Test
    public void test(){
        System.out.println(sysUserService.findByUsername("1"));
    }

    @Test
    public void testPermission(){
        System.out.println(sysPermissionService.list());
    }

    @Test
    public void testUserPermission(){
        System.out.println(sysPermissionService.selectPermissionByUserId(null));
    }


}
