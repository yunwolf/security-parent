package com.yulang.security.core.properties;

public class AuthenticationProperties {

    private String loginPage;

    private String loginProcessingUrl;

    private String usernameParam;

    private String passwordParam;

    private String[] staticPaths;

    private String codeImage;

    private String mobilePage;

    private String codeMobile;

    private Integer tokenValiditySeconds;

    private boolean responseJson = true;


    public String getCodeImage() {
        return codeImage;
    }

    public void setCodeImage(String codeImage) {
        this.codeImage = codeImage;
    }

    public String getMobilePage() {
        return mobilePage;
    }

    public void setMobilePage(String mobilePage) {
        this.mobilePage = mobilePage;
    }

    public String getCodeMobile() {
        return codeMobile;
    }

    public void setCodeMobile(String codeMobile) {
        this.codeMobile = codeMobile;
    }

    public Integer getTokenValiditySeconds() {
        return tokenValiditySeconds;
    }

    public void setTokenValiditySeconds(Integer tokenValiditySeconds) {
        this.tokenValiditySeconds = tokenValiditySeconds;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }

    public String getLoginProcessingUrl() {
        return loginProcessingUrl;
    }

    public void setLoginProcessingUrl(String loginProcessingUrl) {
        this.loginProcessingUrl = loginProcessingUrl;
    }

    public String getUsernameParam() {
        return usernameParam;
    }

    public void setUsernameParam(String usernameParam) {
        this.usernameParam = usernameParam;
    }

    public String getPasswordParam() {
        return passwordParam;
    }

    public void setPasswordParam(String passwordParam) {
        this.passwordParam = passwordParam;
    }

    public String[] getStaticPaths() {
        return staticPaths;
    }

    public void setStaticPaths(String[] staticPaths) {
        this.staticPaths = staticPaths;
    }

    public boolean isResponseJson() {
        return responseJson;
    }

    public void setResponseJson(boolean responseJson) {
        this.responseJson = responseJson;
    }
}
