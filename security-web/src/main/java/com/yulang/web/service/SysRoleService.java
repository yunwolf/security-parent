package com.yulang.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysUser;

import java.util.List;

public interface SysRoleService extends IService<SysRole> {

    SysRole findById(String roleId);

    void saveOrUpdateRolePermission(SysRole sysRole);

    void delete(Long id);

    List<SysRole> findRoleByUserId(String userId);

}
