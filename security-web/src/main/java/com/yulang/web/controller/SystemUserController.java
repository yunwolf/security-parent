package com.yulang.web.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yulang.security.BaseResult;
import com.yulang.web.pojo.SysRole;
import com.yulang.web.pojo.SysUser;
import com.yulang.web.service.SysRoleService;
import com.yulang.web.service.SysUserRoleService;
import com.yulang.web.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.StringUtils;

import javax.xml.ws.soap.Addressing;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/user")
public class SystemUserController {

    private static final String HTML_PREFIX = "system/user/";

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @PreAuthorize("hasAuthority('sys:user')")
    @GetMapping(value = {"/",""})
    public String user(){
        return HTML_PREFIX + "user-list";
    }

    @PreAuthorize("hasAnyAuthority('sys:user:add', 'sys:user:edit')")
    @GetMapping({"/form","/form/{id}"})
    public String form(@PathVariable(required = false) String id, Model model){
        SysUser sysUser;
        if(StringUtils.isEmpty(id)){
            sysUser = new SysUser();
        }else{
            sysUser = sysUserService.getById(id);
            sysUser.setRoleList(sysRoleService.findRoleByUserId(id));
        }
        model.addAttribute("roleList",sysRoleService.list());
        model.addAttribute("user",sysUser);
        return HTML_PREFIX+"user-form";
    }


    @PreAuthorize("hasAnyAuthority('sys:user:add', 'sys:user:edit')")
    @PutMapping
    public String saveOrUpdate(SysUser sysUser){
        sysUserService.saveOrUpdates(sysUser);
        return "redirect:/user";
    }


    @PreAuthorize("hasAnyAuthority('sys:user:list')")
    @ResponseBody
    @PostMapping("/page")
    public BaseResult page(Page page, SysUser sysuser){
        QueryWrapper<SysUser> query = new QueryWrapper<>();
        query.lambda().like(!StringUtils.isEmpty(sysuser.getUsername()),SysUser::getUsername,sysuser.getUsername());
        query.lambda().eq(!StringUtils.isEmpty(sysuser.getMobile()),SysUser::getMobile,sysuser.getMobile());
        return BaseResult.ok(sysUserService.page(page,query));
    }

    @ResponseBody
    @PreAuthorize("hasAuthority('sys:user:delete')")
    @DeleteMapping("/{id}")
    public BaseResult delete(@PathVariable Long id){
        sysUserService.delete(id);
        return BaseResult.ok(true);
    }


}
