package com.yulang.security.core.provider;

import com.yulang.security.core.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthorizeConfigureProvider implements AuthorizeConfigureProvider{

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void configure(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
        registry.antMatchers(securityProperties.getAuthentication().getLoginPage(),
                securityProperties.getAuthentication().getCodeImage(),
                securityProperties.getAuthentication().getMobilePage(),
                securityProperties.getAuthentication().getCodeMobile())
                .permitAll()
                .anyRequest()
                .authenticated();
    }
}
