package com.yulang.web.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/index")
@Controller
public class IndexController {


    @GetMapping
    public String index(){
        return "index";
    }


    @ResponseBody
    @RequestMapping("/user/info")
    public Object userInfo(Authentication authentication){
        return authentication.getPrincipal();
    }

    @ResponseBody
    @RequestMapping("/user/info2")
    public Object userInfo2(@AuthenticationPrincipal UserDetails userDetails){
        return userDetails;
    }


}
