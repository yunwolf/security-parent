package com.yulang.security.core.mobile;

/**
 * 发送短信验证码默认实现
 */
public class SmsSender implements SendSms {

    /**
     * 短信发送
     * @param mobile
     * @param content
     * @return
     */
    @Override
    public boolean sendSms(String mobile, String content) {
        System.out.println("默认实现，验证码：" + content);
        //todo 具体实现
        return true;
    }
}
