package com.yulang.security.core.filter;

import com.yulang.security.core.controller.CustomLoginController;
import com.yulang.security.core.exception.ValidateCodeException;
import com.yulang.security.core.handler.CustomAuthenticationFailHandler;
import com.yulang.security.core.properties.SecurityProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ImageCodeValidateFilter extends OncePerRequestFilter {

    private final SecurityProperties securityProperties;

    private final CustomAuthenticationFailHandler customAuthenticationFailHandler;

    public ImageCodeValidateFilter(SecurityProperties securityProperties,
                                   CustomAuthenticationFailHandler customAuthenticationFailHandler) {
        this.securityProperties = securityProperties;
        this.customAuthenticationFailHandler = customAuthenticationFailHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        //只校验登录请求
        String loginProcessingUrl = securityProperties.getAuthentication().getLoginProcessingUrl();

        if(StringUtils.equalsIgnoreCase(request.getRequestURI(),loginProcessingUrl) &&
                StringUtils.equalsIgnoreCase(request.getMethod(),"post")){
            try {
                validate(request, response, chain);
                return;
            }catch (AuthenticationException e){
                customAuthenticationFailHandler.onAuthenticationFailure(request,response,e);
                return;
            }
        }
        chain.doFilter(request,response);
    }

    private void validate(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String session = (String)request.getSession().getAttribute(CustomLoginController.SESSION_KEY_IMAGE_CODE);
        String code = request.getParameter("code");
        if(StringUtils.equalsIgnoreCase(session,code)){
            chain.doFilter(request,response);
        }else {
            throw new ValidateCodeException("验证码错误！");
        }
    }
}
