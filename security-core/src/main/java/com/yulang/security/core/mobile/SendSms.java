package com.yulang.security.core.mobile;

/**
 * 短信发送统一接口
 */
public interface SendSms {

    boolean sendSms(String mobile,String content);

}
