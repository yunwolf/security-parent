package com.yulang.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.web.pojo.SysRolePermission;
import com.yulang.web.pojo.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
