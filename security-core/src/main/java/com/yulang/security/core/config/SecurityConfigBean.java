package com.yulang.security.core.config;

import com.yulang.security.core.mobile.SendSms;
import com.yulang.security.core.mobile.SmsSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.session.InvalidSessionStrategy;

@Configuration
public class SecurityConfigBean {

    /**
     * 默认使用SmsSender实例如果有其他的这一个就会失效，会使用其他的实例
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(SendSms.class)
    public SendSms sendSms(){
        return new SmsSender();
    }

    /**
     * session失效的处理类
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(InvalidSessionStrategy.class)
    public InvalidSessionStrategy invalidSessionStrategy(){
        return new MyInvalidSessionStrategy(sessionRegistry());
    }

    @Bean
    public SessionRegistry sessionRegistry(){
        return new SessionRegistryImpl();
    }

}
