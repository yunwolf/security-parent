package com.yulang.security.core.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomAuthorizeConfigureManager implements AuthorizeConfigureManager {

    @Autowired
    private List<AuthorizeConfigureProvider> list;

    @Override
    public void configure(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
        for (AuthorizeConfigureProvider provider : list) {
            provider.configure(registry);
        }
    }
}
